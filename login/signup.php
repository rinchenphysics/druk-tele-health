<?php

    $user = 'root';
    $pass = '';
    $db = 'GroupProject';

    $conn = new mysqli('localhost',$user, $pass, $db);

    function validateCID($data) {

        if ($data >= 10000000000 && $data <100000000000 ) {
            return $data;
        }
        else {
            ?>
                <script>
                    alert("Invalid CID"); 
                    window.location.replace("signup.html");
                </script>  
            <?php
        }
        
      }
      function validatePhoneNumber($data) {

        if ($data >= 17000000 && $data <18000000 ) {
            return $data;
        }
        else if ($data >= 77000000 && $data <78000000 ) {
            return $data;
        }
        else {
            ?>
                <script>
                    alert("Invalid Phone Number"); 
                    window.location.replace("signup.html");
                </script>  
            <?php
        }
        
      }

    if (!$conn) {

    echo "Connection failed!";

    }
    else {

  
        if ($_POST['password'] === $_POST['confirm_password']) {
            $pass = md5($_POST['password']);
        }
        else{
            $flag = false;
            
            ?>
                <script>
                  alert("Password Confirmation failed");
                  window.location.replace("signup.html");
    
                </script>  
                <?php

        }
    
    try {
        $name = $_POST['first_name']." ".$_POST['last_name'];

        $img = '';
        $accountType = 1;
            
        $sql2 = $conn -> prepare("INSERT INTO patient_db (cid, Name, Email, Phone, Password, Picture, Account_Type) VALUES (?,?,?,?,?,?,?)");
        $cid = validateCID($_POST['cid']);
        $email = $_POST['email'];
        $PhoneNo = validatePhoneNumber($_POST['Contact_Number']);

        $sql2->bind_param('issisbi',$cid, $name, $email, $PhoneNo, $pass, $img, $accountType);
        $sql2->execute();

        ?>
            <script>
              alert("Sign Up Successful!");
              window.location.replace("login.html");

            </script>  
            <?php

    } catch (\Throwable $th) {
        ?>
            <script>
              alert("CID is already regestered!"); 
              window.location.replace("signup.html");

            </script>  
            <?php
    }
            
        
    }

?>