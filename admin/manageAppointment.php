<?php
session_start();
if(isset($_SESSION['cid'])){
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
    <style>
       @import url("https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap");:root{--header-height: 3rem;--nav-width: 68px;--first-color: #4723D9;--first-color-light: #AFA5D9;--white-color: #F7F6FB;--body-font: 'Nunito', sans-serif;--normal-font-size: 1rem;--z-fixed: 100}*,::before,::after{box-sizing: border-box}body{position: relative;margin: var(--header-height) 0 0 0;padding: 0 1rem;font-family: var(--body-font);font-size: var(--normal-font-size);transition: .5s}a{text-decoration: none}.header{width: 100%;height: var(--header-height);position: fixed;top: 0;left: 0;display: flex;align-items: center;justify-content: space-between;padding: 0 1rem;background-color: var(--white-color);z-index: var(--z-fixed);transition: .5s}.header_toggle{color: var(--first-color);font-size: 1.5rem;cursor: pointer}.header_img{width: 35px;height: 35px;display: flex;justify-content: center;border-radius: 50%;overflow: hidden}.header_img img{width: 40px}.l-navbar{position: fixed;top: 0;left: -30%;width: var(--nav-width);height: 100vh;background-color: var(--first-color);padding: .5rem 1rem 0 0;transition: .5s;z-index: var(--z-fixed)}.nav{height: 100%;display: flex;flex-direction: column;justify-content: space-between;overflow: hidden}.nav_logo, .nav_link{display: grid;grid-template-columns: max-content max-content;align-items: center;column-gap: 1rem;padding: .5rem 0 .5rem 1.5rem}.nav_logo{margin-bottom: 2rem}.nav_logo-icon{font-size: 1.25rem;color: var(--white-color)}.nav_logo-name{color: var(--white-color);font-weight: 700}.nav_link{position: relative;color: var(--first-color-light);margin-bottom: 1.5rem;transition: .3s}.nav_link:hover{color: var(--white-color)}.nav_icon{font-size: 1.25rem}.show{left: 0}.body-pd{padding-left: calc(var(--nav-width) + 1rem)}.active{color: var(--white-color)}.active::before{content: '';position: absolute;left: 0;width: 2px;height: 32px;background-color: var(--white-color)}.height-100{height:100vh}@media screen and (min-width: 768px){body{margin: calc(var(--header-height) + 1rem) 0 0 0;padding-left: calc(var(--nav-width) + 2rem)}.header{height: calc(var(--header-height) + 1rem);padding: 0 2rem 0 calc(var(--nav-width) + 2rem)}.header_img{width: 40px;height: 40px}.header_img img{width: 45px}.l-navbar{left: 0;padding: 1rem 1rem 0 0}.show{width: calc(var(--nav-width) + 156px)}.body-pd{padding-left: calc(var(--nav-width) + 188px)}}
    </style>
</head>
<body id="body-pd">
    <header class="header" id="header">
        <div class="header_toggle"> <i class='bx bx-menu' id="header-toggle"></i> </div>
        <h2 class="text-primary">Druk Tele Health</h2>
    </header>
    <div class="l-navbar" id="nav-bar">
        <nav class="nav">
            <div> <a href="docDetail.php" class="nav_logo"> <i class='bx bx-layer nav_logo-icon'></i> <span class="nav_logo-name">Manage Account</span> </a>
                <div class="nav_list"> <a href="viewfeedback.php" class="nav_link"> <i class='bx bx-grid-alt nav_icon'></i> <span class="nav_name">Feedback</span> </a>
                <a href="manageAppointment.php" class="nav_logo active"> <i class='bx bx-layer nav_logo-icon'></i> <span class="nav_logo-name">Manage Appointment</span> </a>
                <a href="manageDepartment.php" class="nav_logo"> <i class='bx bx-layer nav_logo-icon'></i> <span class="nav_logo-name">Manage Department</span> </a>
                <a href="doctorRegistration.php" class="nav_link"> <i class='bx bx-user nav_icon'></i> <span class="nav_name">Add Doctor Account</span> </a> 
                <a href="department.php" class="nav_link"> <i class='bx bx-user nav_icon'></i> <span class="nav_name">Register Department</span> </a></div>
            </div> <a href="../login/LogOut.php" class="nav_link"> <i class='bx bx-log-out nav_icon'></i> <span class="nav_name">SignOut</span> </a>
        </nav>
    </div>

    <div class="container mt-5">
        <div class="row">
            <div class="col-md-10 mx-auto">
                <table class="table bg-white rounded border">
                <thead>
                    <tr>
                    <th scope="col">Doctor_ID</th>
                    <th scope="col">Date</th>
                    <th scope="col">Time</th>
                    <th scope="col">Patient_ID</th>
                    <th scope="col">Approve</th>
                    <th scope="col">Decline</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                  require_once "db_connection.php";
                  $query=mysqli_query($conn,"Select * from requestschedule");
                  while($row=mysqli_fetch_assoc($query)){
                  
                    ?>
                    <tr>
                      <td><?php echo $row['DoctorID'];?></td>
                      <td><?php echo $row['date'];?></td>
                      <td><?php 
                                if($row['time']==1){
                                    echo"9:30-10:00 AM";
                                }
                                else if($row['time']==2){
                                    echo"10:00-10:30 AM";
                                }
                                else if($row['time']==3){
                                    echo"10:30-11:00 AM";
                                }
                                else if($row['time']==4){
                                    echo"11:00-11:30 AM";
                                }
                                else if($row['time']==5){
                                    echo"12:00-12:30 PM";
                                }
                                else if($row['time']==6){
                                    echo"12:30-1:00 PM";
                                }
                                else if($row['time']==7){
                                    echo"2:00-2:30 PM";
                                }
                                else if($row['time']==8){
                                    echo"2:30-3:00 PM";
                                }
                                else if($row['time']==9){
                                    echo"3:30-3:30 PM";
                                }
                                else if($row['time']==10){
                                    echo"3:30-4:00 PM";
                                }
                                else if($row['time']==11){
                                    echo"4:00-4:30 PM";
                                }
                                else if($row['time']==12){
                                    echo"4:30-5:00 PM";
                                }
                            ?>
                      </td>
                      <td><?php echo $row['PatientID'];?></td>
                      <td><a href="accept.php?Patient_ID=<?php echo $row['PatientID'];?>&Doctor_id=<?php echo $row['DoctorID'];?>&date=<?php echo $row['date'];?>&time=<?php echo $row['time'];?>">Accept</a></td>
                      <td><a href="delete_process3.php?Doctor_id=<?php echo $row['DoctorID'];?>&date=<?php echo $row['date'];?>&time=<?php $row['time']?>">Decline</a></td>
                    </tr>
                  <?php }
                ?>
                </tbody>
                </table>  
            </div>
        </div>
    </div>
    <script>
        document.addEventListener("DOMContentLoaded", function(event) {
        
        const showNavbar = (toggleId, navId, bodyId, headerId) =>{
        const toggle = document.getElementById(toggleId),
        nav = document.getElementById(navId),
        bodypd = document.getElementById(bodyId),
        headerpd = document.getElementById(headerId)

        // Validate that all variables exist
        if(toggle && nav && bodypd && headerpd){
        toggle.addEventListener('click', ()=>{
        // show navbar
        nav.classList.toggle('show')
        // change icon
        toggle.classList.toggle('bx-x')
        // add padding to body
        bodypd.classList.toggle('body-pd')
        // add padding to header
        headerpd.classList.toggle('body-pd')
        })
        }
        }

        showNavbar('header-toggle','nav-bar','body-pd','header')

        /===== LINK ACTIVE =====/
        const linkColor = document.querySelectorAll('.nav_link')

        function colorLink(){
        if(linkColor){
        linkColor.forEach(l=> l.classList.remove('active'))
        this.classList.add('active')
        }
        }
        linkColor.forEach(l=> l.addEventListener('click', colorLink))

        // Your code to run since DOM is loaded and ready
        });
    </script>
    <?php
}else {
    ?>
  <script>
    alert("Login First!");  
    window.location.replace("../login/login.html");
  </script>  
  <?php
  
  }
    ?>
</body>
</html>