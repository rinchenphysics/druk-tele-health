<?php
session_start();
if(isset($_SESSION['cid'])){
include_once 'db_connection.php';


    $did = $_GET['Doctor_id'];
    $date = $_GET['date'];
    $time = $_GET['time'];
    $sql = $conn->prepare("DELETE FROM requestschedule WHERE `DoctorID` = ? and `date` = ? and `time` = ?");
    $sql->bind_param('isi',$did,$date,$time);

         if ($sql->execute()) {
             echo "Record deleted successfully";
             header('Location:manageAppointment.php');
         } else {
             ?>
             <script>
                alert("Failed to Delete Record"); 
             </script>  
             <?php
            header('Location:manageAppointment.php');
     
         }
         mysqli_close($conn);
} else {
    ?>
  <script>
    alert("Login First!");  
    window.location.replace("../login/login.html");
  </script>  
  <?php
}
?>
</body>
</html>