<?php
session_start();
if (isset($_SESSION['cid'])) {

include_once 'db_connection.php';
function validateCID($data) {

    if ($data >= 10000000000 && $data <100000000000 ) {
        return $data;
    }
    else {
        
        header("Location:doctorRegistration.php?error=invalid CID");
    }
    
  }
  function validatePhoneNumber($data) {

    if ($data >= 17000000 && $data <18000000 ) {
        return $data;
    }
    else if ($data >= 77000000 && $data <78000000 ) {
        return $data;
    }
    else {
    
        header("Location:doctorRegistration.php ? error=invalid phone number");
    }
    
  }

   if(isset($_POST['submit'])){
        $dept = $_POST['department'];
        $sql = "SELECT * FROM `department_db` WHERE `Dept_Name` = '$dept'";
        $result = mysqli_query($conn, $sql);
        
        while ($row = $result->fetch_assoc()) {
            $department=$row['Department_id'];
        }

        $name = $_POST['name'];
        $cid= validateCID($_POST['cid']);
        $number=validatePhoneNumber($_POST['number']);
        $type=$_POST['type'];
        
        $email=$_POST['email'];
        $password=md5($_POST['password']);
        $confirmP=md5($_POST['confirm']);
        $pic="";

        if (isset($cid)) {
            if (isset($number)) {
                
                if($type==="Doctor"){
                    $accType = 2;
                    $query="INSERT into doctor_db values('$cid','$name','$email','$number','$password','$pic','$department', '$accType')";
        
                    if(empty($name) || empty($cid) || empty($number) || empty($type) || empty($department) || empty($email) || empty($password) || empty($confirmP)){
                        header("Location:doctorRegistration.php ? error= Fields cannot be empty");
                    }
                    else if($confirmP!==$password){
                        header("Location:doctorRegistration.php ? error=password does not match");
                    }
                    else {
                        mysqli_query($conn,$query);
                        header('Location:doctorRegistration.php');
                    }
                    
                }
                elseif($type==="Admin"){
                    $accType=3;
                    $query="INSERT into admin_db values('$cid','$name','$email','$number','$password','$pic','$accType')";
                    if(empty($name) || empty($cid) || empty($number) || empty($type) || empty($email) || empty($password) || empty($confirmP)){
                        header("Location:doctorRegistration.php ? error= Fields cannot be empty");
                    }
                    else if($confirmP!==$password){
                        header("Location:doctorRegistration.php ? error=password does not match");
                    }
                    else {
                        mysqli_query($conn,$query);
                        header('Location:doctorRegistration.php');
                    }
                }
            }
            else {
                header("Location:doctorRegistration.php?error=invalid Phone Number");
            }
        }
        else {
            header("Location:doctorRegistration.php?error=invalid CID");
        }

            
    }
}else {
    ?>
    <script>
      alert("Login First!");  
      window.location.replace("../login/login.html");
    </script>  
    <?php

}
      ?>
   
