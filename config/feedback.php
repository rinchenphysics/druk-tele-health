<?php
session_start();
if (isset($_SESSION['cid'])) {

?>
<!DOCTYPE html>
<html>
<head>
  <title>Footer</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
     <style>
	    body{
	    	background-image: url(image/hosp.webp);
	    	background-size:cover;
	    }
	    hr{
	    	background: white;	
	    }

		.contact-form{
			background:#009dc4;
			color:white;
			margin-top: 40px;
			padding: 20px;
			box-shadow: 0px 0px 10px 3px grey;
			opacity: 0.9;
			border-radius: 10px;

		}
		.getintouch{
			color: black;
		}
		.btn{
			border-radius: 15px;
		}
		.contact-form #box1{
			background: white;
			color: black;
		}
		#response
		{
			width:100%;
			text-align: center;
		}
		.inner
		{
			display: inline-block;
			
		}
		.input-icons i {
            position: absolute;
        }
          
        .input-icons {
            width: 100%;
            margin-bottom: 10px;
        }
          
        .icon {
            padding: 10px;
            min-width: 40px;
        }
          
        .input-field {
            width: 100%;
            padding: 10px;
            text-align: center;
        }
        .bg-modal{
            display: flex;
            width: 100%;
            height: 100%;
            background-color:rgba(0,0,0,0.7);
            position: absolute;
            top: 0;
            justify-content: center;
            align-items: center;
            z-index: 1;
            display: none;
        }
        .modal-content{
            border-radius: 260px;
            width:300px;
            height: 300px;
            background-color: white;
            border-radius: 4px;
            align-items: center;
            text-align: center;
            padding: 10px;
            position:relative;
            
          }
          .modal-content img{
              border-radius: 50%;
          }
          .close{
          position: absolute;
          top:0;
          right:14px;
          font-size: 42px;
          transform: rotate(45deg);
          cursor: pointer;
        }

   </style>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.0/css/mdb.min.css" rel="stylesheet">
<script src="../patient/appointment.js"></script>
</head>
<body>
  <nav class="navbar sticky-top navbar-expand-lg bg-info">
    <div class="container">
      <a class="navbar-brand" href="#" style="color: white;">Druk Tele Health</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
   <i class="fas fa-bars"></i>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto w-100 justify-content-end">
          <li class="nav-item active">
            <a class="nav-link" href="../patient/index.php" style="color: white;">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../patient/viewRecord.php" style="color: white;">Medical Record</a>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white;">
                    Appointment
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="../patient/appointment.php">Make Appointments</a>
                    <a class="dropdown-item" href="../patient/viewAppointment.php">View Appointments list</a>
                </li>

                <img src="data:image/jpg;charset=utf8;base64,<?php  require_once 'db1.php';
                $query = $conn->query("select picture from patient_db where cid=$_SESSION[cid]");
                $imgSelect = mysqli_fetch_array($query);
                $pic = $imgSelect['picture'];       
                echo base64_encode($pic); ?>" class="rounded-circle" onclick="a()" width="40px" heihgt="40px" alt="">
        </ul>
      </div>
    </div>
  </nav>
  <div class="bg-modal">
    <div class="modal-content">
      <div class="close" onclick="b()">+</div>
      <div class="upload mt-3">
      <img height="115px" width="115px" class="rounded-circle" src="data:image/jpg;charset=utf8;base64,<?php  require_once 'db1.php';
                $query = $conn->query("select picture from patient_db where cid=$_SESSION[cid]");
                $imgSelect = mysqli_fetch_array($query);
                $pic = $imgSelect['picture'];       
                echo base64_encode($pic); ?>">
      </div>
        <div>
        <a href="../patient/manageprofile.php"><input class="btn btn-primary btn-sm" height="" type="button" value="Profile"></a>
        <a href="../login/LogOut.php"><input class="btn btn-primary btn-sm" height="" type="button" value="Log Out"></a>
        </div>
    </div>
</div>

  <div class="container-fluid contact-form col-8">
    <h1 class="text-center">Send us your feedback!</h1>
    <hr>
  
    <div class="row">
     
         <div id="box1" class="col">
         <form action="addfeed.php" method="post">
         <?php
            if(isset($_GET['error'])){ ?>
              <p clase="text-center"><?php echo $_GET['error']; ?></p>
            
            <?php }
            if(isset($_GET['valid'])){ ?>
              <p clase="text-center"><?php echo $_GET['valid']; ?></p>
            <?php }}
          ?>
        <div class="form-group mt-2">   
          <label>What was the reason for your visit?</label>
          <textarea  class="form-control" rows="3" name="reason"></textarea>
        </div>
        <div class="form-group">
          <label>Do you have any suggestions to make our website better?</label>
          <textarea  class="form-control" rows="3" name="suggestion"></textarea>
        </div>
        <div class="form-group d-flex justify-content-center">
          <input class="btn btn-primary" type="submit" value ="Submit" name="submit">
        </div>
          </div>
         </form>
  
      </div>
  
  </div>
<footer class="page-footer bg-dark">

  <div class="bg-info">
    <div class="container">
    </div>
  </div>

  <div class="container text-center text-md-left mt-5">
    <br>
    <div class="row">
      <div class="col-md-2 mx-auto mb-4">
        <h6 class="text-uppercase font-weight-bold">Terms of condition</h6>
        <hr class="bg-info mb-4 mt-0 d-inline-block mx-auto" style="width: 85px; height: 2px">
       
         <ul class="list-unstyled">
            <li class="my-2"><a href="#">Privacy policy</a></li>        
            <li class="my-2"><a href="#">Terms of Use</a></li>
            <li class="my-2"><a href="#">Disclaimer</a></li>         
          </ul>
      </div>
  
      <div class="col-md-2 mx-auto mb-4">
        <h6 class="text-uppercase font-weight-bold">Useful links</h6>
        <hr class="bg-info mb-4 mt-0 d-inline-block mx-auto" style="width: 110px; height: 2px">
          <ul class="list-unstyled">
            <li class="my-2"><a href="#">Bhutan Portal</a></li>        
            <li class="my-2"><a href="#">WHO</a></li>
            <li class="my-2"><a href="#">JDWNRH</a></li>
            <li class="my-2"> <a href="#">Drug Regulatory Authority</a></li>         
            <li class="my-2"> <a href="#">Bhutan Health and Medical Council</a></li>         
          </ul>
      </div>

      <div class="col-md-3 mx-auto mb-4">
        <h6 class="text-uppercase font-weight-bold">Contact</h6>
        <hr class="bg-info mb-4 mt-0 d-inline-block mx-auto" style="width: 75px; height: 2px">
          <ul class="list-unstyled">
            <li class="my-2"><i class="fas fa-home mr-3"></i> 726, Kawajangsa, Thimphu, Bhutan</li>
            <li class="my-2"><i class="fas fa-envelope mr-3"></i> druktelehealth@gmail.com</li>
            <li class="my-2"><i class="fas fa-phone mr-3"></i> + 975 17957538</li>
            <li class="my-2"><i class="fas fa-print mr-3"></i> + 975 2-322602</li>
          </ul>
      </div>
    </div>
  </div>

  <div class="footer-copyright text-center py-3">
    <p>&copy; Copyright
    <a href="#">druktelehealth.com</a></p>
    <p>Take care of your health always</p>
  </div>
</footer>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.0/js/mdb.min.js"></script>
</body>
</html>