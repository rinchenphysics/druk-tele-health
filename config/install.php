<?php
    include_once('DB.php');
    // Patient Table 
    try{
        
        // insert query
        
        $query_for_table_patient = "CREATE table `Patient_DB`(
        `cid` BIGINT NOT NULL PRIMARY KEY ,
        `Name` varchar(100),
        `Email` varchar(100),
        `Phone` INT NOT NULL UNIQUE,
        `Password` VARCHAR(100) NOT NULL,
        `Picture` LONGBLOB,
        `Account_Type` INT(1) NOT NULL
        
        )";
        
        // prepare query for execution
        
        $stmt = $con->prepare($query_for_table_patient);
        
        // Execute the query
        
        if($stmt->execute()) {
        
            echo "<p>Table PatientDB created!</p>";
        
        } else {
        
            echo "<p>Error in creating PatientDB table</p>";
        
        }

    }
    // show error
    catch(PDOException $exception){

        die('ERROR: ' . $exception->getMessage());

    }

    //Doctor Table

    try{
        
        // insert query
        
        $query_for_table_doctor = "CREATE table `Doctor_DB`(
        `cid` BIGINT NOT NULL PRIMARY KEY,
        `Name` varchar(100),
        `Email` varchar(100),
        `Phone` INT NOT NULL UNIQUE,
        `Password` VARCHAR(100) NOT NULL,
        `Picture` LONGBLOB, 
        `DepartmentID` INT NOT NULL,
        `Account_Type` INT(1) NOT NULL
        
        )";
        
        // prepare query for execution
        
        $stmt = $con->prepare($query_for_table_doctor);
        
        // Execute the query
        
        if($stmt->execute()) {
        
            echo "<p>Table DoctorDB created!</p>";
        
        } else {
        
            echo "<p>Error in creating DoctorDB table</p>";
        
        }

    }
    // show error
    catch(PDOException $exception){

        die('ERROR: ' . $exception->getMessage());

    }
    // Admin Table
    try{
        
        // insert query
        
        $query_for_table_admin = "CREATE table `Admin_DB`(
        `cid` BIGINT NOT NULL PRIMARY KEY,
        `Name` varchar(100),
        `Email` varchar(100),
        `Phone` INT NOT NULL UNIQUE,
        `Password` VARCHAR(100) NOT NULL,
        `Picture` LONGBLOB,
        `Account_Type` INT(1) NOT NULL
        
        )";
        
        // prepare query for execution
        
        $stmt = $con->prepare($query_for_table_admin);
        
        // Execute the query
        
        if($stmt->execute()) {
        
            echo "<p>Table AdminDB created!</p>";
        
        } else {
        
            echo "<p>Error in creating AdminDB table</p>";
        
        }

    }
    // show error
    catch(PDOException $exception){

        die('ERROR: ' . $exception->getMessage());

    }

    // default admin user

    try{
        
        // insert query
        $pass = md5('123');
        
        $query_for_Defult_Admin = "INSERT INTO `admin_db`
            (`cid`, `Name`, `Email`, `Phone`, `Password`, `Picture`, `Account_Type`) 
            VALUES ('123','Default','abc@123','111','$pass','','3');
        )";
        
        // prepare query for execution
        
        $stmt = $con->prepare($query_for_Defult_Admin);
        
        // Execute the query
        
        if($stmt->execute()) {
        
            echo "<p>Table Default Admin user created!</p>";
        
        } else {
        
            echo "<p>Error in creating Default admin user</p>";
        
        }

    }
    // show error
    catch(PDOException $exception){

        die('ERROR: ' . $exception->getMessage());

    }

    //Depatrment Table

    try{
        
        // insert query
        
        $query_for_table_department = "CREATE table `department_DB`(
        `Department_id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `Dept_Name` varchar(100) UNIQUE
        
        )";
        
        // prepare query for execution
        
        $stmt = $con->prepare($query_for_table_department);
        
        // Execute the query
        
        if($stmt->execute()) {
        
            echo "<p>Table Department Table created!</p>";
        
        } else {
        
            echo "<p>Error in creating Department table</p>";
        
        }

    }
    // show error
    catch(PDOException $exception){

        die('ERROR: ' . $exception->getMessage());

    }

    //Medical Record

    try{
        
        // insert query
        
        $query_for_table_record = "CREATE table `Medical_Record_DB`(
        `Record_id` BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        `Patient_ID` BIGINT NOT NULL,
        `date` DATE,
        `Diagnosis` TEXT NOT NULL,
        `Treaatment_Plan` TEXT,
        `Advice` TEXT
        
        )";
        
        // prepare query for execution
        
        $stmt = $con->prepare($query_for_table_record);
        
        // Execute the query
        
        if($stmt->execute()) {
        
            echo "<p>Table Medical Record Table created!</p>";
        
        } else {
        
            echo "<p>Error in creating Madical Record Table table</p>";
        
        }

    }
    // show error
    catch(PDOException $exception){

        die('ERROR: ' . $exception->getMessage());

    }


    //RequestSchedule

    try{
        
        // insert query
        
        $query_for_table_R_schedule = "CREATE table `RequestSchedule`(
        `DoctorID` BIGINT NOT NULL,
        `date` DATE NOT NULL,
        `time` INT NOT NULL,
        `PatientID` BIGINT NOT NULL,
        PRIMARY KEY (DoctorID, date, time)
        )";
        
        // prepare query for execution
        
        $stmt = $con->prepare($query_for_table_R_schedule);
        
        // Execute the query
        
        if($stmt->execute()) {
        
            echo "<p>Table Request Schedule created!</p>";
        
        } else {
        
            echo "<p>Error in creating Schedule Table table</p>";
        
        }

    }
    // show error
    catch(PDOException $exception){

        die('ERROR: ' . $exception->getMessage());

    }

    //Schedule

    try{
        
        // insert query
        
        $query_for_table_schedule = "CREATE table `Schedule`(
        `DoctorID` BIGINT NOT NULL,
        `date` DATE NOT NULL,
        `time` INT NOT NULL,
        `PatientID` BIGINT NOT NULL,
        PRIMARY KEY (DoctorID, date, time)
        )";
        
        // prepare query for execution
        
        $stmt = $con->prepare($query_for_table_schedule);
        
        // Execute the query
        
        if($stmt->execute()) {
        
            echo "<p>Table Schedule created!</p>";
        
        } else {
        
            echo "<p>Error in creating Schedule Table table</p>";
        
        }

    }
    // show error
    catch(PDOException $exception){

        die('ERROR: ' . $exception->getMessage());

    }
    
    //feedback

    try{
            
        // insert query
        
        $query_for_table_feedback = "CREATE table `feedback`(
        `id` INT AUTO_INCREMENT PRIMARY KEY,  
        `cid` BIGINT NOT NULL ,
        `reason` TEXT,
        `suggestion` TEXT
        )";
        
        // prepare query for execution
        
        $stmt = $con->prepare($query_for_table_feedback);
        
        // Execute the query
        
        if($stmt->execute()) {
        
            echo "<p>Table feedback created!</p>";
        
        } else {
        
            echo "<p>Error in creating feedback Table table</p>";
        
        }

    }
    // show error
    catch(PDOException $exception){

        die('ERROR: ' . $exception->getMessage());

    }

    

?>