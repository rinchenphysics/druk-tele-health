<?php
session_start();
if(isset($_SESSION['cid'])){
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/js/bootstrap.bundle.min.js">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.0/css/mdb.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">

    <script src="appointment.js"></script>

<style>
    body {
    background: white;
}

.form-control:focus {
    box-shadow: none;
    border-color: #BA68C8
}

.profile-button {
    background: rgb(13, 181, 214);
    box-shadow: none;
    border: none
}

.profile-button:hover {
    background: #682773
}

.profile-button:focus {
    background: #682773;
    box-shadow: none
}

.profile-button:active {
    background: #682773;
    box-shadow: none
}

.back:hover {
    color: #682773;
    cursor: pointer
}

.labels {
    font-size: 11px
}

.add-experience:hover {
    background: #BA68C8;
    color: #fff;
    cursor: pointer;
    border: solid 1px #BA68C8
}
.bg-modal{
            display: flex;
            width: 100%;
            height: 100%;
            background-color:rgba(0,0,0,0.7);
            position: absolute;
            top: 0;
            justify-content: center;
            align-items: center;
            z-index: 1;
            display: none;
        }
        .modal-content{
            border-radius: 260px;
            width:300px;
            height: 300px;
            background-color: white;
            border-radius: 4px;
            align-items: center;
            text-align: center;
            padding: 10px;
            position:relative;
            
          }
          .modal-content img{
              border-radius: 50%;
          }
          .close{
          position: absolute;
          top:0;
          right:14px;
          font-size: 42px;
          transform: rotate(45deg);
          cursor: pointer;
        }
</style>


    <title>Document</title>
</head>
<body>
<nav class="navbar sticky-top navbar-expand-lg bg-info">
        <div class="container">
          <a class="navbar-brand" href="#" style="color: white;">Druk Tele Health</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
       <i class="fas fa-bars"></i>
      </button>
    
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto w-100 justify-content-end">
              <li class="nav-item active">
                <a class="nav-link" href="dindex.php" style="color: white;">Home <span class="sr-only"></span></a>
              </li>
                  <li class="nav-item">
                    <a class="nav-link" href="record.php" style="color: white;">Update Record</a>
                    <li class="nav-item">
                      <a class="nav-link" href="schedule.php" style="color: white;">Time Scheduled</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="trial.php" style="color: white;">Patient Medical Record</a>
                    </li>
                    <img src="data:image/jpg;charset=utf8;base64,<?php  require_once 'db.php';
                $query = $conn->query("select picture from doctor_db where cid=$_SESSION[cid]");
                $imgSelect = mysqli_fetch_array($query);
                $pic = $imgSelect['picture'];       
                echo base64_encode($pic); ?>" class="rounded-circle ml-2" onclick="a()" width="50px" height="50px" alt="Profile">
              </ul>
          </div>
        </div>
      </nav>
      <div class="bg-modal">
    <div class="modal-content">
      <div class="close" onclick="b()">+</div>
      <div class="upload mt-3">
      <img height="115px" width="115px" class="rounded-circle" src="data:image/jpg;charset=utf8;base64,<?php  require_once 'db.php';
                $query = $conn->query("select picture from doctor_db where cid=$_SESSION[cid]");
                $imgSelect = mysqli_fetch_array($query);
                $pic = $imgSelect['picture'];       
                echo base64_encode($pic); ?>">
      </div>
        <div>
        <a href="manageprofile.php"><input class="btn btn-primary btn-sm" height="" type="button" value="Profile"></a>
        <a href="../login/LogOut.php"><input class="btn btn-primary btn-sm" height="" type="button" value="Log Out"></a>
        </div>
    </div>
</div>



    <div class="container rounded bg-white mt-5 mb-5">
    <form action="upload.php" method="POST" enctype="multipart/form-data">
        <div class="row d-flex justify-content-center">
            <div class="col-sm-3 border">
                <div class="d-flex flex-column align-items-center text-center p-3 py-5">
               
                  <img class="rounded-circle mt-2 box-shadow" width="150px" height="150px" src="data:image/jpg;charset=utf8;base64,<?php  require_once 'db.php';
                $query = $conn->query("select * from doctor_db where cid=$_SESSION[cid]");
                $imgSelect = mysqli_fetch_array($query);
                $pic = $imgSelect['Picture'];       
                echo base64_encode($pic); ?>">
                  <div>
                      <div class="m-2"><?php echo $imgSelect['Email'];?></div>
        
                    <label for="file-input">
                      <i class="fa fa-upload" style="size: large;"></i>
                  </label>
                  <label class="labels">Select image</label>
              
                  <input style="display: none;"id="file-input" type="file" name="image">
                  </div>
                </div>
            </div>
            <div class="col-sm-4 border">

                <div class="p-3 py-5">
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <h4 class="text-right">Manage Profile</h4>
                    </div>
                    <div class="row mt-2">
                        <div class="col-md-12"><label class="labels">Full Name</label><input type="text" class="form-control" placeholder=" <?php echo $imgSelect['Name'];?>" readonly></div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12"><label class="labels">Mobile Number</label><input type="text" class="form-control" placeholder="<?php echo $imgSelect['Phone'];?>" value=""></div>
                    </div>
                    </div>
                    <div class="mt-1 mb-3 text-center"><input type="submit" value="Update" name="submit"></div>
                </div>
                </form>
            </div>
            <!-- <div class="col-md-4">
                <div class="p-3 py-5">
                    <div class="d-flex justify-content-between align-items-center experience"><span>Edit Experience</span><span class="border px-3 p-1 add-experience"><i class="fa fa-plus"></i>&nbsp;Experience</span></div><br>
                    <div class="col-md-12"><label class="labels">Experience in Designing</label><input type="text" class="form-control" placeholder="experience" value=""></div> <br>
                    <div class="col-md-12"><label class="labels">Additional Details</label><input type="text" class="form-control" placeholder="additional details" value=""></div>
                </div>
            </div> -->
        </div>
    </div>
    </div>
    </div>
    <footer class="page-footer bg-dark">

        <div class="bg-info">
          <div class="container">
          </div>
        </div>
      
        <div class="container text-center text-md-left mt-5">
          <br>
          <div class="row">
            <div class="col-md-2 mx-auto mb-4">
              <h6 class="text-uppercase font-weight-bold">Terms of condition</h6>
              <hr class="bg-info mb-4 mt-0 d-inline-block mx-auto" style="width: 85px; height: 2px">
             
              <ul class="list-unstyled">
            <li class="my-2"><a href="../config/terms.html" target="_blank">Privacy policy</a></li>        
            <li class="my-2"><a href="../config/terms.html" target="_blank">Terms of Use</a></li>
            <li class="my-2"><a href="../config/terms.html" target="_blank">Disclaimer</a></li>         
          </ul>
      </div>
  
      <div class="col-md-2 mx-auto mb-4">
        <h6 class="text-uppercase font-weight-bold">Useful links</h6>
        <hr class="bg-info mb-4 mt-0 d-inline-block mx-auto" style="width: 110px; height: 2px">
        <ul class="list-unstyled">
          <li class="my-2"><a href="https://www.gov.bt/" target="_blank">Bhutan Portal</a></li>        
          <li class="my-2"><a href="https://www.who.int/" target="_blank">WHO</a></li>
          <li class="my-2"><a href="https://www.jdwnrh.gov.bt/" target="_blank">JDWNRH</a></li>
          <li class="my-2"> <a href="https://dra.gov.bt/" target="_blank">Drug Regulatory Authority</a></li>         
          <li class="my-2"> <a href="http://www.bmhc.gov.bt/" target="_blank">Bhutan Health and Medical Council</a></li>         
        </ul>
            </div>
      
            <div class="col-md-3 mx-auto mb-4">
              <h6 class="text-uppercase font-weight-bold">Contact</h6>
              <hr class="bg-info mb-4 mt-0 d-inline-block mx-auto" style="width: 75px; height: 2px">
                <ul class="list-unstyled">
                  <li class="my-2"><i class="fas fa-home mr-3"></i> 726, Kawajangsa, Thimphu, Bhutan</li>
                  <li class="my-2"><i class="fas fa-envelope mr-3"></i> druktelehealth@gmail.com</li>
                  <li class="my-2"><i class="fas fa-phone mr-3"></i> + 975 17957538</li>
                  <li class="my-2"><i class="fas fa-print mr-3"></i> + 975 2-322602</li>
                </ul>
            </div>
          </div>
        </div>
      
        <div class="footer-copyright text-center py-3">
          <p>&copy; Copyright
          <a href="#">druktelehealth.com</a></p>
          <p>Take care of your health always</p>
        </div>
      </footer>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.0/js/mdb.min.js"></script>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>

      <?php
}else {
  ?>
  <script>
    alert("Login First!");  
    window.location.replace("../login/login.html");
  </script>  
  <?php

}
      ?>
</body>
</html>

