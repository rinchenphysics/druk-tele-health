<?php
session_start();
if(isset($_SESSION['cid'])){
include_once 'db.php';

if ($_SESSION['cid']==$_POST['patientid']) {
  ?>
      <script>
      alert("You Can't treat yourself!");  
      window.location.replace("../doctor/record.php");
      </script>
      <?php
} else{
    $record_id=0;
    $dateofdiagnosis = $_POST['dateofdiagnosis'];
    $diagnosis= $_POST['diagnosis'];
    $advice = $_POST['advice'];
    $treatmentplan = $_POST['treatmentplan'];
    
    $date = date_create();
    $today = date_format($date, 'Y-m-d');

    if ($dateofdiagnosis <= $today) {

    $flag = FALSE;

    $que = $conn -> query("SELECT * FROM `patient_db`");
    while($data = mysqli_fetch_array($que)){
        if( $_POST['patientid'] ==$data['cid']){
          $patientid = $_POST['patientid'];
          $flag = TRUE;
        }        
    }
    if ($flag == TRUE) {
      $sql = "insert into medical_record_db values('$record_id','$patientid','$dateofdiagnosis','$diagnosis','$treatmentplan','$advice')";
      if (mysqli_query($conn, $sql)){
          ?>
          <script>
          alert("Recorded");  
          window.location.replace("../doctor/record.php");
          </script>
          <?php
      }
      else{
          echo "error occured".mysqli_error($conn);
      }
      mysqli_close($conn);
      }
    
      else {
        ?>
          <script>
          alert("Invalid Patient ID");  
          window.location.replace("../doctor/record.php");
          </script>
          <?php
    }
  }
  else {
    ?>
          <script>
          alert("Invalid Date");  
          window.location.replace("../doctor/record.php");
          </script>
          <?php
  }
}
}
else {
    ?>
  <script>
    alert("Login First!");  
    window.location.replace("../login/login.html");
  </script>  
  <?php
  
  }
?>