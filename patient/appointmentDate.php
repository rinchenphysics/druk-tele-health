<?php
session_start();
if (isset($_SESSION['cid'])) {
 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.0/css/mdb.min.css" rel="stylesheet">
   
    <style>
        .desc{
            border: 1px solid black;
            height: 18vh;
            background-color: rgb(37, 173, 231);
        }
        .doctor-img{
            width: 10%;

        }
        .selection-option{
            width: 50%;
        }
        .calender{
            width: 50%;
        }
        @media (max-width:808px) {
            .parent{
                display: flex;
                flex-direction: column;
                height: auto;
            }
            .selection-option{
                width: 100%;
                /* height: 120vh; */

            }
            .calender{
                width: 100%;
                /* margin-top:80%;
                position:absolute;
                margin-left: 100%; */
            }
            .form{
                width: 100%;
                margin-left: 50%;
            }
            
        }

    </style>

<script src="appointment.js"></script>

<link rel="stylesheet" href="styles.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    .bg-modal{
            display: flex;
            width: 100%;
            height: 100%;
            background-color:rgba(0,0,0,0.7);
            position: absolute;
            top: 0;
            justify-content: center;
            align-items: center;
            z-index: 1;
            display: none;
        }
        .modal-content{
            border-radius: 260px;
            width:300px;
            height: 300px;
            background-color: white;
            border-radius: 4px;
            align-items: center;
            text-align: center;
            padding: 10px;
            position:relative;
            
          }
          .modal-content img{
              border-radius: 50%;
          }
          .close{
          position: absolute;
          top:0;
          right:14px;
          font-size: 42px;
          transform: rotate(45deg);
          cursor: pointer;
        }
        .time {
          border: solid black 2px;
          padding: 10px;
          max-width:200px;
          min-width: 150px;
          margin: 2px;
        }
        
</style>
</head>
<body class="img-fluid img-thumbnail" style="background-size: contain; background-repeat: no-repeat; background-color: rgb(207, 240, 229);">

<nav class="navbar sticky-top navbar-expand-lg bg-info">
    <div class="container">
      <a class="navbar-brand" href="#" style="color: white;">Druk Tele Health</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
   <i class="fas fa-bars"></i>
  </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto w-100 justify-content-end">
          <li class="nav-item active">
            <a class="nav-link" href="index.php" style="color: white;">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="viewRecord.php" style="color: white;">Medical Record</a>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white;">
                    Appointment
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="appointment.php">Make Appointment</a>
                    <a class="dropdown-item" href="viewAppointment.php">View Appointment</a>
                </li>
                <?php  require_once 'db.php';
                $query = $conn->query("select picture from patient_db where cid=$_SESSION[cid]");
                $imgSelect = mysqli_fetch_array($query);
                $pic = $imgSelect['picture'];
                ?>
                <img src="data:image/jpg;charset=utf8;base64,<?php echo base64_encode($pic);?>" class="rounded-circle" onclick="a()" width="40px" height="40px" alt="Profile">
                
        </ul>
      </div>
    </div>
  </nav>
  <div class="bg-modal">
    <div class="modal-content">
      <div class="close" onclick="b()">+</div>
      <div class="upload mt-3">
      <img height="115px" width="115px" class="rounded-circle" src="data:image/jpg;charset=utf8;base64,<?php       
                echo base64_encode($pic); ?>">
      </div>
        <div>
        <a href="manageprofile.php"><input class="btn btn-primary btn-sm" height="" type="button" value="Profile"></a>
        <a href="../login/LogOut.php"><input class="btn btn-primary btn-sm" height="" type="button" value="Log Out"></a>
        </div>
    </div>
</div>
        
<?php include_once('db.php');
        $did = $_SESSION['did'];
        
        $details = "select * from doctor_db where cid = $did";
        $result = mysqli_query($conn, $details);
        // $result1 = mysqli_query($conn,$query);
        // $result1 = mysqli_query($conn,$query1);
        while($row = $result->fetch_assoc()){ 
        $dcid = $_SESSION['did'];

        if($dcid==$did){
        
        $dname = $row['Name'];
        $demail = $row['Email'];
        $dphone = $row['Phone'];
        // $dept = $row['DeptName'];
        ?>

    <div class="container-fluid">
        <div class=" mt-3 container desc d-flex"> 
            <div class="ms-3 mt-3 conatainer doctor-img">
            <img src="data:image/jpg;charset=utf8;base64,<?php  require_once 'db.php';
                $query = $conn->query("select picture from doctor_db where cid=$_SESSION[did]");
                $imgSelect = mysqli_fetch_array($query);
                $pic = $imgSelect['picture'];       
                echo base64_encode($pic); ?>" class="rounded-circle" onclick="a()" width="50px" height="50px" alt="This is ME">
            </div>
            <div class="ms-3 container detail">
                <h4><?php echo"$dname";?></h4>
                <p><?php echo "$demail";?></p>
                <p><?php echo "$dphone";?></p>
            </div>
        </div>
        <?php } ?>
       <?php } 
       ?>
       <!-- Start -->
             
      </div>
          
        <div class="container mt-5">  
            <form class="form" action="timeslot.php" method="get">
              <p class="text-center date-formate"><label for="date">Date</label></p>
                <div class="dateInput container" >
                  <input type="text" placeholder="<?php  if(isset($_GET['date'])){echo $_GET['date'];} else { echo ""; }?>"
                    onfocus="(this.type='date')" class="form-control" name = "date">
                </div>
                </div>
                <div class="mt-3 ">
                  <p class="text-center">
                    <button class="btn-primary" name ="dateSelect">Select Date</button></p> 
                </div>
            </form>
          <form class="form" action="app_Date.php" method="post">
          
            <div class = "container mt-5">
              <h3>Booked Time Slots</h3>
              <div class = "row">
            <?php

              if(isset($_GET['time'])){

              $jsonData = file_get_contents('data1.json');
              $arr = json_decode($jsonData,true);
              
              if($_GET['time']=="YES"){
              foreach ($arr as $key => $value) {
                if ($key == 1 && $value == true) {
                  echo "<div class = 'time'> 09:30 AM - 10:00 AM </div>";
                }
                if ($key == 2 && $value == TRUE) {
                  echo "<div class = 'time'> 10:00 AM - 10:30 AM </div>";
                }
                if ($key == 3 && $value == TRUE) {
                  echo "<div class = 'time'> 10:30 AM - 11:00 AM </div>";
                }
                if ($key == 4 && $value == TRUE) {
                  echo "<div class = 'time'> 11:00 AM - 11:30 AM </div>";
                } 
                if ($key == 5 && $value == TRUE) {
                  echo "<div class = 'time'> 12:00 PM - 12:30 PM </div>";
                } 
                if ($key == 6 && $value == TRUE) {
                  echo "<div class = 'time'> 12:30 PM - 01:00 PM </div>";
                } 
                if ($key == 7 && $value == TRUE) {
                  echo "<div class = 'time'> 02:00 PM - 02:30 PM </div>";
                } 
                if ($key == 8 && $value == TRUE) {
                  echo "<div class = 'time'> 02:30 PM - 03:00 PM </div>";
                } 
                if ($key == 9 && $value == TRUE) {
                  echo "<div class = 'time'> 03:00 PM - 03:30 PM </div>";
                }
                if ($key == 10 && $value == TRUE) {
                  echo "<div class = 'time'> 03:30 PM - 04:00 PM </div>";
                }
                if ($key == 11 && $value == TRUE) {
                  echo "<div class = 'time'> 04:00 PM - 04:30 PM </div>";
                }
                if ($key == 12 && $value == TRUE) {
                  echo "<div class = 'time'> 04:30 PM - 05:00 PM </div>";
                }
              }
            } else{
              echo "<div class = 'time'><h3>No Time Slot Occupied</h3></div>";
            }
          }

            ?>
            </div>
            </div>            
                
            <div class='mt-5 container slot-select'>
              <h4>Select Slots(Slot time 30 minutes)</h4>
            </div>

            <div class=' parent mt-2 container  d-flex'>
            <div class=' mt-3 selection-option'>
              <input type="hidden"  name ="date" value="<?php if(isset($_GET['date'])){echo $_GET['date'];}?>">
              <select name = "timeslot">
                <option value="1">09:30 AM - 10:00 AM</option>
                <option value="2">10:00 AM - 10:30 AM</option>
                <option value="3">10:30 AM - 11:00 AM</option>
                <option value="4">11:00 AM - 11:30 AM</option>
                <option value="5">12:00 PM - 12:30 PM</option>
                <option value="6">12:30 PM - 01:00 PM</option>
                <option value="7">02:00 PM - 02:30 PM</option>
                <option value="8">02:30 PM - 03:00 PM</option>
                <option value="9">03:00 PM - 03:30 PM</option>
                <option value="10">03:30 PM - 04:00 PM</option>
                <option value="11">04:00 PM - 04:30 PM</option>
                <option value="12">04:30 PM - 05:00 PM</option>
              </select>
            </div>

            </div>
            <div class="mt-3 ">
              <p class="text-center"><input class="btn-primary" type="submit" key="Confirm"></p> 
            </div>

          </form>

                </div>
                
        
        <!-- Stop -->
    </div> 
    
    <footer class="page-footer bg-dark">

        <div class="bg-info">
          <div class="container">
          </div>
        </div>
      
        <div class="container text-center text-md-left mt-5">
          <br>
          <div class="row">
            <div class="col-md-2 mx-auto mb-4">
              <h6 class="text-uppercase font-weight-bold">Terms of condition</h6>
              <hr class="bg-info mb-4 mt-0 d-inline-block mx-auto" style="width: 85px; height: 2px">
             
              <ul class="list-unstyled">
            <li class="my-2"><a href="../config/terms.html" target="_blank">Privacy policy</a></li>        
            <li class="my-2"><a href="../config/terms.html" target="_blank">Terms of Use</a></li>
            <li class="my-2"><a href="../config/terms.html" target="_blank">Disclaimer</a></li>         
          </ul>
      </div>
  
      <div class="col-md-2 mx-auto mb-4">
        <h6 class="text-uppercase font-weight-bold">Useful links</h6>
        <hr class="bg-info mb-4 mt-0 d-inline-block mx-auto" style="width: 110px; height: 2px">
        <ul class="list-unstyled">
          <li class="my-2"><a href="https://www.gov.bt/" target="_blank">Bhutan Portal</a></li>        
          <li class="my-2"><a href="https://www.who.int/" target="_blank">WHO</a></li>
          <li class="my-2"><a href="https://www.jdwnrh.gov.bt/" target="_blank">JDWNRH</a></li>
          <li class="my-2"> <a href="https://dra.gov.bt/" target="_blank">Drug Regulatory Authority</a></li>         
          <li class="my-2"> <a href="http://www.bmhc.gov.bt/" target="_blank">Bhutan Health and Medical Council</a></li>         
        </ul>
            </div>
      
            <div class="col-md-3 mx-auto mb-4">
              <h6 class="text-uppercase font-weight-bold">Contact</h6>
              <hr class="bg-info mb-4 mt-0 d-inline-block mx-auto" style="width: 75px; height: 2px">
                <ul class="list-unstyled">
                <li class="my-2"><i class="fas fa-home mr-3"></i> 726, Kawajangsa, Thimphu, Bhutan</li>
                <li class="my-2"><i class="fas fa-envelope mr-3"></i> druktelehealth@gmail.com</li>
                <li class="my-2"><i class="fas fa-envelope mr-3"></i><a href="../config/feedback.php">Feedback</a></li>
                <li class="my-2"><i class="fas fa-phone mr-3"></i> + 975 17957538</li>
                <li class="my-2"><i class="fas fa-print mr-3"></i> + 975 2-322602</li>
                </ul>
            </div>
          </div>
        </div>
      
        <div class="footer-copyright text-center py-3">
          <p>&copy; Copyright
          <a href="#">druktelehealth.com</a></p>
          <p>Take care of your health always</p>
        </div>
      </footer>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.0/js/mdb.min.js"></script>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>

    <script>
        
    </script>
    <?php
}else {
  ?>
  <script>
    alert("Login First!");  
    window.location.replace("../login/login.html");
  </script>  
  <?php
  
  }
    ?>
</body>
</html>