<?php
session_start();
    require_once('db.php');
    if(isset($_SESSION['cid']) && isset($_SESSION['did'])){
        $did = $_SESSION['did'];
        $date = $_POST['date'];
        echo $date;
        $time = $_POST['timeslot'];
        $pid = $_SESSION['cid'];
        $sql2 = "SELECT * FROM schedule";
        $sql_result = mysqli_query($conn, $sql2);
        $today = date_create();
        $current = date_format($today, 'Y-m-d');
        date_add($today,date_interval_create_from_date_string("7 days"));
        $limit = date_format($today, 'Y-m-d');

    $query2 = $conn -> query("SELECT * FROM `schedule` WHERE PatientID = $pid and date >= '$current'");
    $query3 = $conn -> query("SELECT * FROM `requestschedule` WHERE PatientID = $pid and date >= '$current'");
    $count = 0;
    $schedule =[];

    while($data = mysqli_fetch_array($query2)){
        $schedule[$count]=$data['time'];
        $count++;                
    }

    while($data2 = mysqli_fetch_array($query3)){
        $schedule[$count]=$data2['time'];
        $count++;                
    }
    if(empty($schedule)){
        
    if ($did!=$pid) {
        if($date < $limit){
            if($date > $current){
            while ($row = mysqli_fetch_assoc($sql_result)) {
                if ($row['DoctorID'] == $did && $row['date']==$date && $row['time']==$time) {
                    ?>
                    <script>
                        alert("Unavailable Time Slot"); 
                        window.location.replace('../patient/appointmentDate.php');
                    </script>  
                    <?php
                    
                }
            }
            try {
                $sql = $conn->prepare("INSERT INTO `requestschedule`(`DoctorID`, `date`, `time`, `PatientID`) VALUES (?,?,?,?)");
                $sql->bind_param('isii',$did,$date,$time,$pid);
                $sql->execute();
        
                ?>
                    <script>
                        alert("Request Sent");
                        window.location.replace('../patient/appointmentDate.php'); 
                    </script>  
                    <?php
                    
            } catch (\Throwable $th) {
                ?>
                    <script>
                        alert("Pending Request"); 
                        window.location.replace('../patient/appointmentDate.php');
                    </script>  
                    <?php
                    
            }
        }else {
            ?>
                    <script>
                        alert("Date exceded"); 
                        window.location.replace('../patient/appointmentDate.php');
                    </script>  
                    <?php
        }
    }
        else{
            ?>
                    <script>
                        alert("Date Limit exceded"); 
                        window.location.replace('../patient/appointmentDate.php');
                    </script>  
                    <?php
                    
        
        }
    }
    else {
        ?>
                    <script>
                        alert("You Cannot book an appointment with yourself!");
                        window.location.replace('../patient/appointmentDate.php'); 
                    </script>  
                    <?php
                            
    }
    } else {
        ?>
            <script>
                        alert("You Can Only book one appointment at a time!");
                        window.location.replace('../patient/appointmentDate.php'); 
                    </script>  
                    <?php
    }
    
}
?>