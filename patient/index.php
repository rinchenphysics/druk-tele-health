<?php
session_start();
if (isset($_SESSION['cid'])) {
  
?>
<!DOCTYPE html>
<html>
<head>
  <title>About Us Page</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/all.min.css">
  
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.0/css/mdb.min.css" rel="stylesheet">
<script src="appointment.js"></script>

<style>
    .bg-modal{
            display: flex;
            width: 100%;
            height: 100%;
            background-color:rgba(0,0,0,0.7);
            position: absolute;
            top: 0;
            justify-content: center;
            align-items: center;
            z-index: 1;
            display: none;
        }
        .modal-content{
            border-radius: 260px;
            width:300px;
            height: 300px;
            background-color: white;
            border-radius: 4px;
            align-items: center;
            text-align: center;
            padding: 10px;
            position:relative;
            
          }
          .modal-content img{
              border-radius: 50%;
          }
          .close{
          position: absolute;
          top:0;
          right:14px;
          font-size: 42px;
          transform: rotate(45deg);
          cursor: pointer;
        }
</style>
</head>
<body class="img-fluid img-thumbnail" style="background-image: url('Images/Aboutus.jpg'); background-size: contain; background-repeat: no-repeat; background-color: rgb(207, 240, 229);">
<nav class="navbar sticky-top navbar-expand-lg bg-info">
    <div class="container">
      <a class="navbar-brand" href="#" style="color: white;">Druk Tele Health</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
   <i class="fas fa-bars"></i>
  </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto w-100 justify-content-end">
          <li class="nav-item active">
            <a class="nav-link" href="index.php" style="color: white;">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="viewRecord.php" style="color: white;">Medical Record</a>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white;">
                    Appointment
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="appointment.php">Make Appointment</a>
                    <a class="dropdown-item" href="viewAppointment.php">View Appointment</a>
                </li>
                <?php  require_once 'db.php';
                $query = $conn->query("select picture from patient_db where cid=$_SESSION[cid]");
                $imgSelect = mysqli_fetch_array($query);
                $pic = $imgSelect['picture'];
                ?>
                <img src="data:image/jpg;charset=utf8;base64,<?php echo base64_encode($pic);?>" class="rounded-circle" onclick="a()" width="40px" height="40px" alt="Profile">
                
        </ul>
      </div>
    </div>
  </nav>
  <div class="bg-modal">
    <div class="modal-content">
      <div class="close" onclick="b()">+</div>
      <div class="upload mt-3">
      <img height="115px" width="115px" class="rounded-circle" src="data:image/jpg;charset=utf8;base64,<?php  require_once 'db.php';
                $query = $conn->query("select picture from patient_db where cid=$_SESSION[cid]");
                $imgSelect = mysqli_fetch_array($query);
                $pic = $imgSelect['picture'];       
                echo base64_encode($pic); ?>">
      </div>
        <div>
        <a href="manageprofile.php"><input class="btn btn-primary btn-sm" height="" type="button" value="Profile"></a>
        <a href="../login/LogOut.php"><input class="btn btn-primary btn-sm" height="" type="button" value="Log Out"></a>
        </div>
    </div>
</div>

<!-- Enter your body code here-->
<div class="container">
  <div class="row">

    <div class="col-md-6">
        <h1 style="color: red; padding-top: 15%;">Druk Tele Health</h1>
        <h3 style="padding-top: 5%;">Providing the Best Medical Solutions</h3>
          <p>Aims to assist individuals in monitoring their own health <br> 
              conditions and help to keep track of medications.
          </p>
          <p>
              Allows healthcare providers to share and report on a <br> 
              patient's personal health records remotely.
          </p>
        <img src="" alt="">
    </div>

    <div class="col-md-6">
  
    </div>

  </div>

</div>
  <!-- End of your Body code-->

<footer class="page-footer bg-dark">

  <div class="bg-info">
    <div class="container">
    </div>
  </div>

  <div class="container text-center text-md-left mt-5">
    <br>
    <div class="row">
      <div class="col-md-2 mx-auto mb-4">
        <h6 class="text-uppercase font-weight-bold">Terms of condition</h6>
        <hr class="bg-info mb-4 mt-0 d-inline-block mx-auto" style="width: 85px; height: 2px">
       
         <ul class="list-unstyled">
            <li class="my-2"><a href="../config/terms.html" target="_blank">Privacy policy</a></li>        
            <li class="my-2"><a href="../config/terms.html" target="_blank">Terms of Use</a></li>
            <li class="my-2"><a href="../config/terms.html" target="_blank">Disclaimer</a></li>         
          </ul>
      </div>
  
      <div class="col-md-2 mx-auto mb-4">
        <h6 class="text-uppercase font-weight-bold">Useful links</h6>
        <hr class="bg-info mb-4 mt-0 d-inline-block mx-auto" style="width: 110px; height: 2px">
        <ul class="list-unstyled">
          <li class="my-2"><a href="https://www.gov.bt/" target="_blank">Bhutan Portal</a></li>        
          <li class="my-2"><a href="https://www.who.int/" target="_blank">WHO</a></li>
          <li class="my-2"><a href="https://www.jdwnrh.gov.bt/" target="_blank">JDWNRH</a></li>
          <li class="my-2"> <a href="https://dra.gov.bt/" target="_blank">Drug Regulatory Authority</a></li>         
          <li class="my-2"> <a href="http://www.bmhc.gov.bt/" target="_blank">Bhutan Health and Medical Council</a></li>         
        </ul>
      </div>

      <div class="col-md-3 mx-auto mb-4">
        <h6 class="text-uppercase font-weight-bold">Contact</h6>
        <hr class="bg-info mb-4 mt-0 d-inline-block mx-auto" style="width: 75px; height: 2px">
          <ul class="list-unstyled">
            <li class="my-2"><i class="fas fa-home mr-3"></i> 726, Kawajangsa, Thimphu, Bhutan</li>
            <li class="my-2"><i class="fas fa-envelope mr-3"></i> druktelehealth@gmail.com</li>
            <li class="my-2"><i class="fas fa-envelope mr-3"></i><a href="../config/feedback.php">Feedback</a></li>
            <li class="my-2"><i class="fas fa-phone mr-3"></i> + 975 17957538</li>
            <li class="my-2"><i class="fas fa-print mr-3"></i> + 975 2-322602</li>
          </ul>
      </div>
    </div>
  </div>

  <div class="footer-copyright text-center py-3">
    <p>&copy; Copyright
    <a href="#">druktelehealth.com</a></p>
    <p>Take care of your health always</p>
  </div>
</footer>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.0/js/mdb.min.js"></script>
</body>
</html>
<?php
}
else {
  ?>
  <script>
    alert("Login First!");  
    window.location.replace("../login/login.html");
  </script>  
  <?php
}
?>